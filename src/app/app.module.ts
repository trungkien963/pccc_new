import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { BookPage } from '../pages/book/book';
import { TabsPage } from '../pages/tabs/tabs';
import { HistoryBookPage } from '../pages/history-book/history-book';
import { NotificationPage } from '../pages/notification/notification';
import { ServicesPricePage } from '../pages/services-price/services-price';
import { DetailHistoryPage } from '../pages/detail-history/detail-history';
import { ContactPage } from '../pages/contact/contact';
import { AccountPage } from '../pages/account/account';
import { TechniciansPage } from '../pages/technicians/technicians';
import { Tab2Page } from '../pages/tab2/tab2';
import { MonthTask1Page } from '../pages/month-task1/month-task1';
import { DetailTaskPage } from '../pages/detail-task/detail-task';
import { HistoryTaskPage } from '../pages/history-task/history-task';
import { PayPage } from '../pages/pay/pay';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { NotificationHistoryPage } from '../pages/notification-history/notification-history';

import { ServiceProvider } from '../services/GlobalVar';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import { MediaCapture } from '@ionic-native/media-capture';
import { Camera } from '@ionic-native/camera';
import { PayPal } from '@ionic-native/paypal';
import { Push } from '@ionic-native/push';
import { Keyboard } from '@ionic-native/keyboard';
import { CallNumber } from '@ionic-native/call-number';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BookPage,
    TabsPage,
    HistoryBookPage,
    NotificationPage,
    ServicesPricePage,
    DetailHistoryPage,
    ContactPage,
    AccountPage,
    TechniciansPage,
    Tab2Page,
    MonthTask1Page,
    DetailTaskPage,
    HistoryTaskPage,
    PayPage,
    SignUpPage,
    NotificationHistoryPage
  ],
  imports: [
    HttpModule,
    CalendarModule,
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BookPage,
    TabsPage,
    HistoryBookPage,
    NotificationPage,
    ServicesPricePage,
    DetailHistoryPage,
    ContactPage,
    AccountPage,
    TechniciansPage,
    Tab2Page,
    MonthTask1Page,
    DetailTaskPage,
    HistoryTaskPage,
    PayPage,
    SignUpPage,
    NotificationHistoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ServiceProvider,
    ImagePicker,
    File,
    Camera,
    MediaCapture,
    PayPal,
    Push,
    Keyboard,
    CallNumber,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
