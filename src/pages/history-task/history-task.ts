import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-history-task',
  templateUrl: 'history-task.html',
})
export class HistoryTaskPage {
  history: any = [];
  isNull = false;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  ionViewWillEnter() {

    if (this.serviceProvider.currentUser != '') {

      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      this.serviceProvider.getHistoryTask().subscribe((data) => {
        loader.dismiss();
        let res = <any>data;

        if (res.length == undefined) {
          this.isNull = true;
        }
        else {
          this.history = res;
          console.log(res.length);
        }
      }, (res) => {
        alert("Lỗi hệ thống! Vui lòng thử lại sau");
        loader.dismiss();
      })
    }
    else {
      this.history = [];
    }

  }

  toDetail(i) {
    var data = this.history[i];
    this.navCtrl.push(DetailTaskPage, { dt: data });
  }

}
