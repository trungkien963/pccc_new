import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailHistoryPage } from '../detail-history/detail-history';
import { ServiceProvider } from '../../services/GlobalVar';
import { Storage } from '@ionic/storage';
import { ServicesPricePage } from '../services-price/services-price';
import moment from 'moment';

@Component({
  selector: 'page-history-book',
  templateUrl: 'history-book.html',
})
export class HistoryBookPage {

  // orders = orders.data;
  orders = [];
  isNull = false;
  constructor(public loadingCtrl: LoadingController, public storage: Storage, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  ionViewWillEnter() {
    this.orders = [];
    // this.storage.remove("BookData")
    this.storage.get("BookData").then(res => {

      if (res != null) {
        const loader = this.loadingCtrl.create({
          content: "Vui lòng chờ..."
        });
        loader.present();

        for (let order of JSON.parse(res)) {
          this.serviceProvider.getOrder(order).subscribe(res => {
            this.orders.push(res);
            console.log(res);
          })
        }

        if (this.orders.length == JSON.parse(res).length) {
          for (var i = 0; i < this.orders.length; i++) {
            console.log("kien", this.orders.length);
            if (this.orders[i + 1] != undefined) {
              if (moment(this.orders[i].data_item.creatingDate) > moment(this.orders[i + 1].data_item.creatingDate)) {
                let tempt = this.orders[i];
                this.orders[i] = this.orders[i + 1];
                this.orders[i + 1] = tempt;
              }
            }
          }
        }


        loader.dismiss();
        console.log(this.orders)
      }
      else {
        this.isNull = true;
      }
    })


    console.log('ionViewDidLoad HistoryBookPage', this.serviceProvider.orders);
  }

  doRefresh(refresher) {
    this.orders = [];
    this.storage.get("BookData").then(res => {

      if (res != null) {
        const loader = this.loadingCtrl.create({
          content: "Vui lòng chờ..."
        });
        loader.present();

        for (let order of JSON.parse(res)) {
          this.serviceProvider.getOrder(order).subscribe(res => {
            this.orders.push(res);
          })
        }

        if (this.orders.length == JSON.parse(res).length) {
          for (var i = 0; i < this.orders.length; i++) {
            console.log("kien", this.orders.length);
            if (this.orders[i + 1] != undefined) {
              if (moment(this.orders[i].data_item.creatingDate) > moment(this.orders[i + 1].data_item.creatingDate)) {
                let tempt = this.orders[i];
                this.orders[i] = this.orders[i + 1];
                this.orders[i + 1] = tempt;
              }
            }
          }
        }

        refresher.complete();
        loader.dismiss();
        console.log(this.orders)
      }
      else {
        this.isNull = true;
      }
    })
  }


  toDetail(v: number) {
    var data = this.orders[v].data_item;
    console.log(this.orders[v])
    // this.navCtrl.push(DetailHistoryPage, { dt: data });
    this.navCtrl.push(ServicesPricePage, { dt: data, id: v });
  }
}
