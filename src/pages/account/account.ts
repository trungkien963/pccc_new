import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { JwtHelperService } from "@auth0/angular-jwt";
import * as jwt_decode from "jwt-decode";
// import { Routes } from '@angular/router';

import { ServiceProvider } from '../../services/GlobalVar';

import { Tab2Page } from '../tab2/tab2';
import { SignUpPage } from '../sign-up/sign-up';
import { TabsPage } from '../tabs/tabs';

// const helper = new JwtHelperService();
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})


export class AccountPage {
  username: any;
  password: any;
  isLogin = false;
  technician: any = {};
  loginErr = false;
  name: string;

  constructor(public alertController: AlertController, private app: App, public navCtrl: NavController, public navParams: NavParams,
    public serviceProvider: ServiceProvider, public localStorage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
    this.serviceProvider.getTechnician().subscribe(data => {
      let res = <any>data;
      this.technician = res.data;
    });

    this.localStorage.get("Account").then(res => {
      console.log(res);
      if (res != null && res.status == 1) {
        this.serviceProvider.currentUser = res.username;
        this.name = res.name;
        this.isLogin = true;
        this.serviceProvider.isLogin = true;
      }
    })
  }

  onLogin() {

    // for (let technician of this.technician) {
    //   if (technician.data_item.content.phone == this.username) {
    //     let account = {
    //       username: technician.data_item.content.id,
    //       name: technician.data_item.content.name,
    //       status: 1
    //     }
    //     this.localStorage.set("Account", account);
    //     console.log('Access!');
    //     this.name = technician.data_item.content.name;
    //     this.isLogin = true;
    //     this.serviceProvider.isLogin = true;
    //     this.serviceProvider.currentUser = technician.data_item.content.id;
    //     this.navCtrl.setRoot(Tab2Page);
    //     this.navCtrl.popToRoot();
    //     break;
    //   }
    // }

    // if (this.isLogin == false) {
    //   this.loginErr = true;
    // }

    let body = {
      username: this.username,
      password: this.password
    }

    this.serviceProvider.login(body).subscribe(data => {
      console.log("info", data);
      let dt = <any>data;
      console.log(dt.token);

      const info = jwt_decode(dt.token);
      // localStorage.set("access_token", dt.token);

      console.log("decode", info);
      let account = {
        username: info.information.username,
        name: info.information.last_name + " " + info.information.first_name,
        roles: info.roles,
        address: info.information.address,
        status: 1
      }
      this.localStorage.set("Account", account);
      console.log('Access!');
      this.name = info.information.last_name + " " + info.information.first_name;
      this.isLogin = true;
      this.loginErr = false;
      this.serviceProvider.isLogin = true;
      this.serviceProvider.currentUser = info.information.phone_number;
      if (info.roles == "technician") {
        this.navCtrl.setRoot(Tab2Page);
        this.navCtrl.popToRoot();
        this.serviceProvider.isCustomer = false;
      }
    },
      err => {
        console.log(err);
        this.loginErr = true
      })
  }

  async onLogout() {
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'Bạn có chắc muốn đăng xuất không?',
      buttons: [
        {
          text: 'Hủy',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.localStorage.remove("Account");
            this.isLogin = false;
            this.app.getRootNav().setRoot(TabsPage);
            this.navCtrl.popToRoot();
            this.serviceProvider.currentUser = '';
            this.serviceProvider.isLogin = false;
            this.serviceProvider.isCustomer = true;
            // let root= this.app.getRootNav();
            // root.popToRoot();
          }
        }
      ]
    });

    await alert.present();
  }

  onClickSignUp() {
    this.navCtrl.push(SignUpPage);
  }

  // ionViewDidLeave() {
  //   this.navCtrl.popToRoot();
  // }

}
