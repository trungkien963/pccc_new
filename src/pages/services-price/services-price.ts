import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { startTimeRange } from '@angular/core/src/profile/wtf_impl';
import { ServiceProvider } from "../../services/GlobalVar";
import { Storage } from '@ionic/storage';
import moment from 'moment';

@Component({
  selector: 'page-services-price',
  templateUrl: 'services-price.html',
})
export class ServicesPricePage {

  data: any = [];
  id: number;
  note = '';
  txtcounter: number;
  isEdit: boolean = false;
  price: any;
  dataTest: any;
  hasContract = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private viewCtrl: ViewController, private payPal: PayPal, public localStorage: Storage,
    public alertController: AlertController, public serviceProvider: ServiceProvider) {
    this.data = this.navParams.get('dt');
    this.id = this.navParams.get('id');
    this.deleteSymbol();
    console.log("dtaaaa", this.data);
    console.log(this.id);

  }

  deleteSymbol() {
    let str = this.data.content.orderValue.sum.split(".");
    
    let price='';
    for(let a of str){
      price=price+a;
    }

    this.price = Number(price) / 24000;
  }

  // acceptOrders() {
  //   if (this.data.content.paymentInfo.paymentMethod == '1') {
  //     this.makePayment();
  //   }
  //   else {
  //     if (this.hasContract == 0) {
  //       this.serviceProvider.updateOrder(this.data.content.id, "\"3\"").subscribe((data) => {
  //         this.viewCtrl.dismiss();
  //         this.presentAlertNoti();
  //         this.updateStatus(this.data.content.id, "\"3\"");
  //         this.serviceProvider.editNotification(this.data.content.id, "\"3\"");
  //       }, (err) => {
  //         alert("Update trạng thái fail!");
  //       });
  //     }

  //     else {
  //       this.serviceProvider.updateOrder(this.data.content.id, "\"2a\"").subscribe((data) => {
  //         this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {
  //           this.viewCtrl.dismiss();
  //           this.presentAlertNoti();
  //           this.updateStatus(this.data.content.id, "\"2a\"");
  //           this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
  //         }, (err) => {
  //           alert("Update trạng thái fail!");
  //           console.log(err)
  //         });
  //       }, (err) => {
  //         alert("Update trạng thái fail!");
  //       });
  //     }
  //   }
  // }

  acceptOrders() {
    if (this.hasContract == 1) {
      if (this.data.content.paymentInfo.paymentMethod == '1') {
        // this.makePayment();
        let paymentInfo = {
          "invoice": {},
          "paymentDate": "",
          "paymentMethod": "0",
          "status": "0"
        };

        this.serviceProvider.updateOrderPayment(this.data.content.id, paymentInfo).subscribe((res) => {
          this.serviceProvider.updateOrder(this.data.content.id, "\"2a\"").subscribe((data) => {
            this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {

              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2a",
                  orderID: this.data.content.id,
                  msg: `Khách hàng đã chấp nhận báo giá`
                },
                isNew: "1"
              };
          
              let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;
          
              this.serviceProvider.publishMessage(newMessage, id);

              this.viewCtrl.dismiss();
              this.presentAlertNoti();
              this.updateStatus(this.data.content.id, "\"2a\"");
              this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
            }, (err) => {
              alert("Update trạng thái fail!");
              console.log(err)
            });
          }, (err) => {
            alert("Update trạng thái fail!");
          });
        }, (err) => {
          alert("Update trạng thái fail!");
        });
      }
      else {
        this.serviceProvider.updateOrder(this.data.content.id, "\"2a\"").subscribe((data) => {
          this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {

            
            let newMessage = {
              from: "mobile",
              to: "crm",
              datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
              content: {
                mobileChannel: this.serviceProvider.idOfDevice,
                currentOrderStatus: "2a",
                orderID: this.data.content.id,
                msg: `Khách hàng đã chấp nhận báo giá`
              },
              isNew: "1"
            };
        
            let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;
        
            this.serviceProvider.publishMessage(newMessage, id);


            this.viewCtrl.dismiss();
            this.presentAlertNoti();
            this.updateStatus(this.data.content.id, "\"2a\"");
            this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
          }, (err) => {
            alert("Update trạng thái fail!");
            console.log(err)
          });
        }, (err) => {
          alert("Update trạng thái fail!");
        });
      }
    }

    if (this.hasContract == 0) {
      if (this.data.content.paymentInfo.paymentMethod == '1') {
        this.makePayment();
      }
      else {
        if (this.hasContract == 0) {
          this.serviceProvider.updateOrder(this.data.content.id, "\"3\"").subscribe((data) => {
            
            let newMessage = {
              from: "mobile",
              to: "crm",
              datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
              content: {
                mobileChannel: this.serviceProvider.idOfDevice,
                currentOrderStatus: "3",
                orderID: this.data.content.id,
                msg: `Khách hàng đã chấp nhận báo giá`
              },
              isNew: "1"
            };
        
            let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;
        
            this.serviceProvider.publishMessage(newMessage, id);

            this.viewCtrl.dismiss();
            this.presentAlertNoti();
            this.updateStatus(this.data.content.id, "\"3\"");
            this.serviceProvider.editNotification(this.data.content.id, "\"3\"");
          }, (err) => {
            alert("Update trạng thái fail!");
          });
        }
      }
    }

  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }

  editOrders() {
    this.isEdit = !this.isEdit;
  }

  cancelOrders() {
    // this.viewCtrl.dismiss();
    this.presentAlertConfirm();

  }

  // submit() {
  //   if (this.note == '') {
  //     this.acceptOrders();
  //   }
  //   else {
  //     if (this.data.content.paymentInfo.paymentMethod == '1') {
  //       this.makePayment();
  //     }
  //     else {
  //       // this.data.content.status = "2";
  //       this.serviceProvider.updateOrderNote(this.data.content.id, this.note).subscribe((data1) => {
  //         this.serviceProvider.updateOrder(this.data.content.id, "\"2\"").subscribe((data) => {
  //           if (this.hasContract == 1) {
  //             this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {
  //               alert("Sửa lịch hẹn thành công!");
  //               this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
  //               this.updateStatus(this.data.content.id, "\"2\"");
  //               this.navCtrl.pop();
  //             }, (err) => {
  //               alert("Update trạng thái fail!");
  //               console.log(err)
  //             });
  //           }
  //           else {
  //             alert("Sửa lịch hẹn thành công!");
  //             this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
  //             this.updateStatus(this.data.content.id, "\"2\"");
  //             this.navCtrl.pop();
  //           }
  //         }, (err) => {
  //           alert("Update trạng thái fail!");
  //           console.log(err)
  //         });
  //       }, (err) => {
  //         alert("Update note fail!")
  //       });
  //     }
  //   }

  // }

  submit() {
    if (this.note == '') {
      this.acceptOrders();
    }

    else {
      if (this.hasContract == 0) {
        if (this.data.content.paymentInfo.paymentMethod == '1') {
          this.makePayment();
        }
        else {
          // this.data.content.status = "2";
          this.serviceProvider.updateOrderNote(this.data.content.id, this.note).subscribe((data1) => {
            this.serviceProvider.updateOrder(this.data.content.id, "\"2\"").subscribe((data) => {

              
              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  msg: `Đơn hàng ${this.data.content.id} đã phản hồi, cần chỉnh sửa`
                },
                isNew: "1"
              };
          
              let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;
          
              this.serviceProvider.publishMessage(newMessage, id);


              alert("Sửa lịch hẹn thành công!");
              this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
              this.updateStatus(this.data.content.id, "\"2\"");
              this.navCtrl.pop();
            }, (err) => {
              alert("Update trạng thái fail!");
              console.log(err)
            });
          }, (err) => {
            alert("Update note fail!")
          });
        }
      }

      if (this.hasContract == 1) {
        if (this.data.content.paymentInfo.paymentMethod == '1') {
          // this.makePayment();
          let paymentInfo = {
            "invoice": {},
            "paymentDate": "",
            "paymentMethod": "0",
            "status": "0"
          };

          this.serviceProvider.updateOrderPayment(this.data.content.id, paymentInfo).subscribe((res) => {
            this.serviceProvider.updateOrderNote(this.data.content.id, this.note).subscribe((data1) => {
              this.serviceProvider.updateOrder(this.data.content.id, "\"2\"").subscribe((data) => {
                this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {

                  let newMessage = {
                    from: "mobile",
                    to: "crm",
                    datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                    content: {
                      mobileChannel: this.serviceProvider.idOfDevice,
                      currentOrderStatus: "2",
                      orderID: this.data.content.id,
                      msg: `Đơn hàng ${this.data.content.id} đã phản hồi, cần chỉnh sửa`
                    },
                    isNew: "1"
                  };
              
                  let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
              
                  this.serviceProvider.publishMessage(newMessage, id);

                  alert("Sửa lịch hẹn thành công!");
                  this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
                  this.updateStatus(this.data.content.id, "\"2\"");
                  this.navCtrl.pop();
                }, (err) => {
                  alert("Update trạng thái fail!");
                  console.log(err)
                });
              }, (err) => {
                alert("Update trạng thái fail!");
                console.log(err)
              });
            }, (err) => {
              alert("Update note fail!")
            });
          })
        }
        else {
          // this.data.content.status = "2";
          this.serviceProvider.updateOrderNote(this.data.content.id, this.note).subscribe((data1) => {
            this.serviceProvider.updateOrder(this.data.content.id, "\"2\"").subscribe((data) => {
              this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {

                let newMessage = {
                  from: "mobile",
                  to: "crm",
                  datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                  content: {
                    mobileChannel: this.serviceProvider.idOfDevice,
                    currentOrderStatus: "2",
                    orderID: this.data.content.id,
                    msg: `Đơn hàng ${this.data.content.id} đã phản hồi, cần chỉnh sửa`
                  },
                  isNew: "1"
                };
            
                let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
            
                this.serviceProvider.publishMessage(newMessage, id);

                alert("Sửa lịch hẹn thành công!");
                this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
                this.updateStatus(this.data.content.id, "\"2\"");
                this.navCtrl.pop();
              }, (err) => {
                alert("Update trạng thái fail!");
                console.log(err)
              });
            }, (err) => {
              alert("Update trạng thái fail!");
              console.log(err)
            });
          }, (err) => {
            alert("Update note fail!")
          });
        }
      }

    }

  }

  closeOrders() {
    this.viewCtrl.dismiss();
  }

  makePayment() {
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AX2hkFGXhcGafHJLNmglLfCgsruAe0WpCCot2H-W6ZfEIdhnuFZdW6qzmuye1t79wgW5hg4Yp8Yxfcme'
    }).then(() => {
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        merchantName: 'PCCC',
        acceptCreditCards: true,
        payPalShippingAddressOption: 0,
        rememberUser: true,
        languageOrLocale: 'vi_VN'

      })).then(() => {
        // let payment = new PayPalPayment(this.price, 'VND', this.data.order.orderID, 'thanh toán cho đơn hàng này ', 'sale');
        let payment = new PayPalPayment(`${this.price}`, 'USD', `ID Đơn hàng:${this.data.content.id}`, 'sale');
        console.log(payment)
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          console.log(payment)

          let paymentInfo = {
            "invoice": res,
            "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
            "paymentMethod": "1",
            "status": "1"
          }

          if (this.note != '') {
            this.data.content.status = "2";
            this.data.content.note = this.note;
            this.serviceProvider.updateOrder(this.data.content.id, "\"2\"").subscribe((res) => {
              this.serviceProvider.updateOrderPayment(this.data.content.id, paymentInfo).subscribe((res) => {
                console.log('Update payment thành công')
                this.serviceProvider.updateOrderNote(this.data.content.id, this.note).subscribe((data) => {

                  let newMessage = {
                    from: "mobile",
                    to: "crm",
                    datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                    content: {
                      mobileChannel: this.serviceProvider.idOfDevice,
                      currentOrderStatus: "2",
                      orderID: this.data.content.id,
                      msg: `Đơn hàng ${this.data.content.id} đã thanh toán, cần chỉnh sửa`
                    },
                    isNew: "1"
                  };
              
                  let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
              
                  this.serviceProvider.publishMessage(newMessage, id);

                    alert('Thanh toán thành công!');
                    this.updateStatus(this.data.content.id, "\"2\"");
                    this.navCtrl.pop();
                    this.serviceProvider.editNotification(this.data.content.id, "\"2\"");

                }, (err) => {
                  alert('Update chỉnh sửa k thành công!');
                });

              }, (err) => {
                alert('Update phương thức thanh toán fail!');
              }
              );
            }, (err) => {
              alert("Update trạng thái fail!")
            });


          }
          else {
            // if (this.hasContract == 0) {
              this.data.content.status = "6";
              this.serviceProvider.updateOrder(this.data.content.id, "\"6\"").subscribe(data => {

                this.serviceProvider.updateOrderPayment(this.data.content.id, paymentInfo).subscribe(
                  (res) => {

                    let newMessage = {
                      from: "mobile",
                      to: "crm",
                      datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                      content: {
                        mobileChannel: this.serviceProvider.idOfDevice,
                        currentOrderStatus: "2",
                        orderID: this.data.content.id,
                        msg: `Đơn hàng ${this.data.content.id} đã thanh toán, cần phân công`
                      },
                      isNew: "1"
                    };
                
                    let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
                
                    this.serviceProvider.publishMessage(newMessage, id);

                    console.log({ message: 'updatePaymentInfo Result', res: res });
                    alert('Thanh toán thành công!');
                    this.updateStatus(this.data.content.id, "\"6\"");
                    this.serviceProvider.editNotification(this.data.content.id, "\"6\"");
                    this.navCtrl.pop();
                  },
                  (err) => console.log(err),
                  () => console.log('done!')
                );;

              }, (err) => {
                alert('Fail! Vui lùng thử lại!')
              });
            // }
            // else {
            //   this.data.content.status = "2a";
            //   this.serviceProvider.updateOrder(this.data.content.id, "\"2a\"").subscribe(data => {

            //     this.serviceProvider.updateOrderPayment(this.data.content.id, paymentInfo).subscribe(
            //       (res) => {
            //         this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {
            //           console.log({ message: 'updatePaymentInfo Result', res: res });
            //           alert('Thanh toán thành công!');
            //           this.updateStatus(this.data.content.id, "\"2a\"");
            //           this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
            //           this.navCtrl.pop();
            //         }, (err) => {
            //           alert("Update trạng thái fail!");
            //           console.log(err)
            //         });

            //       },
            //       (err) => console.log(err),
            //       () => console.log('done!')
            //     );;

            //   }, (err) => {
            //     alert('Fail! Vui lùng thử lại!')
            //   });
            // }

          }

          // this.serviceProvider.updateOrder(this.data.content.id, this.data.content);
        }, (err) => {
          // Error or render dialog closed without being successful
          alert('Thanh toán fail!')
          console.log(err)
        });
      }, (err) => {
        // Error in configuration
        alert('Thanh toán fail!')
        console.log(err)
      });
    }, (err) => {
      // Error in initialization, maybe PayPal isn't supported or something else
      alert('Thanh toán fail!')
      console.log(err)
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      title: 'Xác nhận',
      message: 'Bạn có chắc muốn hủy đặt lịch này không?',
      buttons: [
        {
          text: 'Hủy bỏ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.serviceProvider.updateOrder(this.data.content.id, "\"7\"").subscribe((data) => {
              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  msg: `Đơn hàng ${this.data.content.id} đã hủy`
                },
                isNew: "1"
              };
          
              let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
          
              this.serviceProvider.publishMessage(newMessage, id);
              console.log(data);
              this.viewCtrl.dismiss();
            });
            this.updateStatus(this.data.content.id, "\"7\"");
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertNoti() {
    const alert = await this.alertController.create({
      title: 'Xác nhận',
      message: 'Bạn đã đặt lịch thành công',
      buttons: [
        {
          text: 'YES',
          handler: () => {
            // this.navCtrl.popToRoot();
          }
        }
      ]
    });

    await alert.present();
  }

  async updateStatus(id: string, status: string) {
    var data: any;
    await this.localStorage.get("notificationHistory").then(res => {
      data = JSON.parse(res);
    });

    for (var i = 0; i < data.length; i++) {
      if (data[i].order.content.id == id) {
        data[i].order.content.status = status;
      }
    }

    return await this.localStorage.set("notificationHistory", JSON.stringify(data));
  }


  changeContractType(num) {
    if (num == 1) {
      this.hasContract = 1;
    }
    if (num == 0) {
      this.hasContract = 0;
    }

    console.log("has Contarct", this.hasContract);
  }

  changeType() {
    console.log("has Contarct", this.hasContract);
  }
}
