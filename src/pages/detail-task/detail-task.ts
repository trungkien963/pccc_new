import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { PayPage } from '../pay/pay';
import { ServiceProvider } from '../../services/GlobalVar';
import moment from 'moment';

@Component({
  selector: 'page-detail-task',
  templateUrl: 'detail-task.html',
})
export class DetailTaskPage {
  data: any;
  check = false;
  isPayment = false;
  isFinishTask = false;
  status: string;

  constructor(public toastController: ToastController, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider) {
    this.data = this.navParams.get('dt');
    console.log(this.data.id);
    this.status = this.data.content.status;
    
    if (moment(this.data.content.assignInfo.assignee.deadline, 'DD/MM/YYYY HH:mm:ss').format("DD/MM/YYYY") >= moment().format("DD/MM/YYYY")
      && moment().format("DD/MM/YYYY") >= moment(this.data.content.assignInfo.assignee.workDate, 'DD/MM/YYYY HH:mm:ss').format("DD/MM/YYYY")) {
      this.check = true;
    }

    if (this.data.content.status == "4" || this.data.content.status == "5") {
      this.isFinishTask = true;
    }
    if (this.data.content.status == "5" || this.data.content.status == "6") {
      this.isPayment = true;
    }
  }

  confirmFinish() {
    if (this.status == "3") {
      console.log("kien", this.data.content.id)
      this.status = "4";
      let status = "\"4\"";
      this.serviceProvider.updateOrder(this.data.id, status).subscribe(data => {

        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "4",
            orderID: this.data.content.id,
            msg: `Đơn hàng ${this.data.content.id} đã hoàn tất công việc, chưa thanh toán`
          },
          isNew: "1"
        };
    
        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
    
        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id);

        this.isFinishTask = true;
        let toast = this.toastController.create({
          message: 'Bạn đã hoàn thành công việc',
          duration: 3000
        });

        toast.present();
        // this.navCtrl.push(PayPage, { dt: this.data });
      });
    }

    if (this.status == "6") {
      this.status = "5";
      let status = "\"5\"";
      this.serviceProvider.updateOrder(this.data.id, status).subscribe(data => {

        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "5",
            orderID: this.data.content.id,
            msg: `Đơn hàng ${this.data.content.id} đã hoàn tất công việc, đã thanh toán`
          },
          isNew: "1"
        };
    
        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
    
        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id);

        this.isFinishTask = true;
        let toast = this.toastController.create({
          message: 'Bạn đã hoàn thành công việc',
          duration: 3000
        });

        toast.present();
        // this.navCtrl.push(PayPage, { dt: this.data });
      });
    }


  }

  confirmPayment() {

    if (this.status == "3") {
      this.status = "6";
      let status = "\"6\"";
      let paymentStatus = "\"1\"";
      let paymentInfo = {
        "invoice": {},
        "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
        "paymentMethod": "0",
        "status": "1"
      }
      this.serviceProvider.updateOrder(this.data.id, status).subscribe(data => {
        this.serviceProvider.updateOrderPayment(this.data.id, paymentInfo).subscribe(data => {
          let newMessage = {
            from: "mobile",
            to: "crm",
            datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
            content: {
              mobileChannel: this.serviceProvider.idOfDevice,
              currentOrderStatus: "6",
              orderID: this.data.content.id,
              msg: `Đơn hàng ${this.data.content.id} đã thanh toán, chưa hoàn tất công việc`
            },
            isNew: "1"
          };
      
          let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
      
          this.serviceProvider.publishMessage(newMessage, id);
          this.serviceProvider.publishMessagetoCustomer(newMessage, id);

          this.isPayment = true;
          let toast = this.toastController.create({
            message: 'Bạn đã nhận thanh toán',
            duration: 3000
          });

          toast.present();

          // this.navCtrl.pop();
        }, (err) => {
          alert("update thanh toán không thành công")
        })
      }, err => {
        alert("update trang thái không thành công")
      });
    }

    if (this.status == "4") {
      this.status = "5";
      let status = "\"5\"";
      let paymentStatus = "\"1\"";
      let paymentInfo = {
        "invoice": {},
        "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
        "paymentMethod": "0",
        "status": "1"
      }
      this.serviceProvider.updateOrder(this.data.id, status).subscribe(data => {
        this.serviceProvider.updateOrderPayment(this.data.id, paymentInfo).subscribe(data => {
          let newMessage = {
            from: "mobile",
            to: "crm",
            datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
            content: {
              mobileChannel: this.serviceProvider.idOfDevice,
              currentOrderStatus: "5",
              orderID: this.data.content.id,
              msg: `Đơn hàng ${this.data.content.id} đã hoàn tất công việc, đã thanh toán`
            },
            isNew: "1"
          };
      
          let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
      
          this.serviceProvider.publishMessage(newMessage, id);
          this.serviceProvider.publishMessagetoCustomer(newMessage, id);

          this.isPayment = true;
          let toast = this.toastController.create({
            message: 'Bạn đã nhận thanh toán',
            duration: 3000
          });

          toast.present();

          // this.navCtrl.pop();
        }, (err) => {
          alert("update thanh toán không thành công")
        })
      }, err => {
        alert("update trang thái không thành công")
      });
    }


  }
}
