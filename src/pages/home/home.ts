import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { BookPage } from '../book/book';
import { ServiceProvider } from '../../services/GlobalVar';
import { ServicesPricePage } from '../services-price/services-price';
import { AccountPage } from '../account/account';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  data: any = {};
  services: any;
  display = false;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider) {

    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.serviceProvider.getServices().subscribe(dt => {

      let res = <any>dt;
      this.services = new Array(12);
      console.log(res.data)
      this.services = res.data.sort((a, b) => {
        let t1 = a.data_item.content.index;
        let t2 = b.data_item.content.index;

        if (t1 > t2) {
          return 1;
        }
        if (t1 < t2) {
          return -1;
        }

        return 0;
        // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
      });
      console.log("serv", this.services);
      console.log(this.services);
      this.display = true;
      loader.dismiss();
    }, (res) => {
      alert("Lỗi hệ thống! Vui lòng thử lại sau");
      loader.dismiss();
    });
  }

  doRefresh(refresher) {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.serviceProvider.getServices().subscribe(dt => {
      let res = <any>dt;
      // this.services = res.data;
      this.services = new Array(12);
      console.log(res.data)
      // this.services = res.data;
      this.services = res.data.sort((a, b) => {
        let t1 = a.data_item.content.index;
        let t2 = b.data_item.content.index;

        if (t1 > t2) {
          return 1;
        }
        if (t1 < t2) {
          return -1;
        }

        return 0;
        // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
      });
      console.log(this.services);
      loader.dismiss();
      refresher.complete();
    }, (res) => {
      alert("Lỗi hệ thống! Vui lòng thử lại sau");
      loader.dismiss();
      refresher.complete();
    });
  }

  ionViewWillEnter() {
    this.data.image = "https://i.imgur.com/U4Oadyu.png";
    this.data.image2 = "https://i.imgur.com/r7uK5St.jpg";
  }

  goBookPage(service: any) {
    this.serviceProvider.selectedServiceList = [];
    this.serviceProvider.selectedServiceList.push(service);
    console.log("ffff", this.serviceProvider.selectedServiceList);
    this.navCtrl.push(BookPage);
  }

  gotoAccount() {
    this.navCtrl.push(AccountPage);
  }
}
