import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import moment from 'moment';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-technicians',
  templateUrl: 'technicians.html',
})
export class TechniciansPage {
  tasks: any = [];
  currDate = moment().format("MM/DD/YYYY");
  todaytasks: any;
  checkNull = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController,
    public loadingCtrl: LoadingController, public serviceProvider: ServiceProvider) {

  }

  ionViewWillEnter() {
    if (this.serviceProvider.currentUser != '') {

      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      this.serviceProvider.getTodayTask().subscribe((data) => {
        loader.dismiss();
        let res = <any>data;
        if (res.length == undefined) {
          this.checkNull = true;
        }
        else {
          this.tasks = res;
          console.log(this.tasks);
          this.checkNull = false;
        }

      }, (res) => {
        alert("Lỗi hệ thống! Vui lòng thử lại sau");
        loader.dismiss();
      })
    }
    else {
      this.checkNull = true;
      this.tasks = [];
    }


  }

  toServicesPrice(v: number) {
    var data = this.tasks[v];
    this.navCtrl.push(DetailTaskPage, { dt: data });

    console.log("dfgd", data)
  }

}
