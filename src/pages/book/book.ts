import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, ActionSheetController, LoadingController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { ServiceProvider } from '../../services/GlobalVar';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { File, FileEntry } from '@ionic-native/file';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture';
import { Camera, CameraOptions } from '@ionic-native/camera';
import moment from 'moment';
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from "@angular/http";
import sdk from '../../sdk/index';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-book',
  templateUrl: 'book.html',
})
export class BookPage {
  @ViewChild(Slides) slides: Slides;
  // @ViewChild('myvideo') myVideo:any;
  services = [];
  selectedService = [];
  txtcounter: number;
  note = "";
  imageResponse = [];
  video = '';
  phone: string;
  phoneCusDir: string;
  name: string;
  address: string;
  addressCusDir: string;
  nameCusDir: string;
  selectedRadio: string = "cash";
  masterFormdataToSend = new FormData();
  payments = [
    { paymentType: 'cash', paymentStatusCode: '0', paymentStatusName: 'Trả sau bằng tiền mặt', img: '../../assets/imgs/cash.png' },
    { paymentType: 'card', paymentStatusCode: '1', paymentStatusName: 'Trả trước qua cổng thanh toán (Master VISA)', img: '../../assets/imgs/card.png' }
  ];
  // datetime: String = new Date().toISOString();
  // datetime: String = new Date(new Date().setHours(new Date().getHours()-17)).toISOString();
  datetime = moment().format();
  serviceIsNULL = false;
  dateTimeErr = false;
  nameErr = false;
  nameIsNULL = false;
  nameCusDirErr = false;
  nameCusDirIsNULL = false;
  phoneErr = false;
  phoneIsNULL = false;
  phoneCusDirErr = false;
  phoneCusDirIsNULL = false;
  addressIsNULL = false;
  addressCusDirIsNULL = false;

  deviceID: string;
  id: string;
  isCusDir = 1;
  displayCusDir = false;

  provinces = [];
  selectedProvince = "79";
  provinceName = "Thành phố Hồ Chí Minh";
  districts = [];
  selectedDistrict;
  districtName: string;
  districtIsNULL = false;
  wards = [];
  selectedWard;
  wardName: string;
  wardIsNULL = false;

  provincesCusDir = [];
  selectedProvinceCusDir = "79";
  provinceNameCusDir = "Thành phố Hồ Chí Minh";
  districtsCusDir = [];
  selectedDistrictCusDir;
  districtNameCusDir: string;
  districtIsNULLCusDir = false;
  wardsCusDir = [];
  selectedWardCusDir;
  wardNameCusDir: string;
  wardIsNULLCusDir = false;
  mobileChannel;

  payment = { paymentType: 'cash', paymentStatusCode: '0', paymentStatusName: 'Trả sau bằng tiền mặt' };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public serviceProvider: ServiceProvider, public imagePicker: ImagePicker,
    public file: File, private mediaCapture: MediaCapture, public camera: Camera,
    public toastController: ToastController, public actionSheetController: ActionSheetController,
    private sanitizer: DomSanitizer, public localStorage: Storage,
    public loadingCtrl: LoadingController, private http: Http) {
    // this.publishMessage();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookPage');
    let currTime = moment().format("YYYYMMDDHHmmssms").toString();
    // this.localStorage.remove('BookData');
    this.slides.lockSwipeToNext(true);
    this.selectedService.push(this.serviceProvider.selectedServiceList[0].id);
    this.serviceProvider.getServices().subscribe(dt => {
      let res = <any>dt;
      this.services = res.data;
      console.log("ssss", this.services);
    });

    this.localStorage.get("DeviceToken").then(res => {
      console.log(res);
      this.deviceID = res;
      if (res == null || res == "fakeDeviceID") {
        this.deviceID = 'fakeDeviceID';
      }
      // else {
      //   this.id = res + currTime;
      // }
    });

    this.localStorage.get("MobileChannel").then(res => {
      this.mobileChannel = res;
      if (res == null || res == "fakeDeviceID") {
        this.mobileChannel = 'fakeDeviceID';
      }
    })

    this.localStorage.get("Account").then(res => {
      console.log("Kết quả", res);
      if (res != null) {
        let address = res.address.split(", ");
        console.log("address", address);
        let addressDetail = address.slice(0, -3);
        let nameAddress = "";
        for (var i = 0; i < addressDetail.length; i++) {
          if (addressDetail[i + 1] != undefined) {
            nameAddress = nameAddress + addressDetail[i] + ", ";
          }
          else {
            nameAddress = nameAddress + addressDetail[i];
          }
        }

        console.log("Kiên", addressDetail);
        console.log("Kiên", nameAddress);

        this.name = res.name;
        this.phone = res.username;
        this.provinceName = address[address.length - 1];
        this.districtName = address[address.length - 2];
        this.wardName = address[address.length - 3];
        this.address = nameAddress;

      }
    })

    this.serviceProvider.getProvinces().subscribe(data => {
      console.log("Tỉnh thành", data.json());
      this.provinces = data.json().data_item;
      this.provincesCusDir = data.json().data_item;
      for (var i = 0; i < this.provinces.length; i++) {
        if (this.provinces[i].province_code == "79") {
          let temp = this.provinces[0];
          this.provinces[0] = this.provinces[i];
          this.provinces[i] = temp;
        }
        if (this.provinces[i].province_code == "01") {
          let temp = this.provinces[1];
          this.provinces[1] = this.provinces[i];
          this.provinces[i] = temp;
        }
      }

      for (var i = 0; i < this.provincesCusDir.length; i++) {
        if (this.provincesCusDir[i].province_code == "79") {
          let temp = this.provinces[0];
          this.provincesCusDir[0] = this.provincesCusDir[i];
          this.provincesCusDir[i] = temp;
        }
        if (this.provincesCusDir[i].province_code == "01") {
          let temp = this.provincesCusDir[1];
          this.provincesCusDir[1] = this.provincesCusDir[i];
          this.provincesCusDir[i] = temp;
        }
      }
    });

    this.changeProvince(this.selectedProvince);
    this.changeProvinceCusDir(this.selectedProvinceCusDir);
  }

  changeInfo() {
    console.log("kien");
    if (this.isCusDir == 0) {
      this.displayCusDir = true;
    }
    else {
      this.displayCusDir = false;
    }
  }

  cusDirChange(num) {
    if (num == 1) {
      this.isCusDir = 1;
    }
    if (num == 0) {
      this.isCusDir = 0;
    }
  }

  nextStep2() {
    console.log("slice", this.slides.getActiveIndex())

    console.log(this.checkDateTime());
    if (this.dateTimeErr == false && this.serviceIsNULL == false) {
      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      let services = {
        service_name: this.selectedService
      }

      this.serviceProvider.getGenerateID(services).subscribe((data) => {
        // console.log(data);
        let result = <any>data;
        if (result.code == 0) {
          for (var i = 0; i < result.message.length; i++) {
            if (i == 0) {
              this.id = result.message[i];
            }
            if (i > 0) {
              this.id = this.id + '-' + result.message[i];
            }
          }

          this.slides.lockSwipeToNext(false);
          this.slides.lockSwipeToPrev(false);
          this.slides.slideTo(this.slides.getActiveIndex() + 1);
          this.slides.lockSwipeToNext(true);
          this.slides.lockSwipeToPrev(true);

          loader.dismiss();
        }
        else {
          alert("Lỗi hệ thống! Vui lòng thử lại sau...");
          loader.dismiss();
        }

        console.log(this.id)
      });
    }

  }

  nextStep() {

    console.log("slice", this.slides.getActiveIndex());
    if (this.name == null) {
      this.nameIsNULL = true;
    }

    if (this.phone == null) {
      this.phoneIsNULL = true;
    }

    if (this.address == null) {
      this.addressIsNULL = true;
    }

    if (this.districtName == '') {
      this.districtIsNULL = true;
    }

    if (this.wardName == '') {
      this.wardIsNULL = true;
    }


    if (this.isCusDir == 1) {
      if (this.wardIsNULL == false && this.districtIsNULL == false && this.addressIsNULL == false && this.nameIsNULL == false && this.addressIsNULL == false && this.nameErr == false && this.phoneErr == false) {
        const loader = this.loadingCtrl.create({
          content: "Vui lòng chờ..."
        });
        loader.present();

        this.slides.lockSwipeToNext(false);
        this.slides.lockSwipeToPrev(false);
        this.slides.slideTo(this.slides.getActiveIndex() + 1);
        this.slides.lockSwipeToNext(true);
        this.slides.lockSwipeToPrev(true);
        console.log(this.checkDateTime());
        console.log(this.phone);

        this.nameCusDir = this.name;
        this.phoneCusDir = this.phone;
        this.addressCusDir = this.address;

        this.districtNameCusDir = this.districtName;
        this.provinceNameCusDir = this.provinceName;
        this.wardNameCusDir = this.wardName;

        loader.dismiss();
      }
    }
    else {
      if (this.addressCusDir == null) {
        this.addressCusDirIsNULL = true;
      }

      if (this.districtNameCusDir == '') {
        this.districtIsNULLCusDir = true;
      }

      if (this.wardNameCusDir == '') {
        this.wardIsNULLCusDir = true;
      }
      if (this.wardIsNULLCusDir == false && this.districtIsNULLCusDir == false && this.wardIsNULL == false && this.districtIsNULL == false && this.addressIsNULL == false && this.nameIsNULL == false && this.addressCusDirIsNULL == false && this.nameErr == false && this.phoneErr == false) {
        const loader = this.loadingCtrl.create({
          content: "Vui lòng chờ..."
        });
        loader.present();

        this.slides.lockSwipeToNext(false);
        this.slides.lockSwipeToPrev(false);
        this.slides.slideTo(this.slides.getActiveIndex() + 1);
        this.slides.lockSwipeToNext(true);
        this.slides.lockSwipeToPrev(true);
        console.log(this.checkDateTime());
        console.log(this.phone);

        loader.dismiss();
      }
    }


  }

  back() {
    this.slides.lockSwipeToPrev(false);
    this.slides.slidePrev();
    this.slides.lockSwipeToPrev(true);
  }

  changeServices() {
    console.log(this.selectedService);
    this.serviceProvider.selectedServiceList = [];
    for (var i = 0; i < this.selectedService.length; i++) {
      for (var j = 0; j < this.services.length; j++) {
        if (this.selectedService[i] == this.services[j].data_item.content.id) {
          this.serviceProvider.selectedServiceList.push(this.services[j].data_item.content);
          break;
        }
      }
    }

    if (this.serviceProvider.selectedServiceList.length == 0) {
      this.serviceIsNULL = true;
    }
    else {
      this.serviceIsNULL = false;
    }
    console.log(this.serviceProvider.selectedServiceList);
  }

  checkDateTime() {
    var currDateTime = moment(moment().format());
    var diffTime = moment.duration(currDateTime.diff(this.datetime)).asMilliseconds();
    console.log(currDateTime);
    console.log(this.datetime);
    console.log(diffTime);

    if (diffTime > 15000) {
      this.dateTimeErr = true;
      // this.datetime = moment().format();
    }
    else {
      this.dateTimeErr = false;
    }
  }

  checkName(event) {
    var nameArr = [];
    var nameArrNew = event.split('');
    let isNum = 0;
    let isSymbol = 0;

    if (nameArrNew.length == 0) {
      this.nameIsNULL = true;
    }
    else {
      this.nameIsNULL = false;
    }

    for (var i = 0; i < nameArrNew.length; i++) {
      if (nameArrNew[i] != " ") {
        nameArr.push(nameArrNew[i]);
      }
    }
    for (var i = 0; i < nameArr.length; i++) {
      if (isNaN(Number(nameArr[i].toString())) == false) {
        isNum += 1;
      }
      if (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(nameArr[i])) {
        isSymbol += 1;
      }
    }

    if (isNum > 0 || isSymbol > 0) {
      this.nameErr = true;
    }
    else {
      this.nameErr = false;
      isSymbol = 0;
      isNum = 0;
    }

  }

  checkNameCusDir(event) {
    var nameArr = [];
    var nameArrNew = event.split('');
    let isNum = 0;
    let isSymbol = 0;

    if (nameArrNew.length == 0) {
      this.nameCusDirIsNULL = true;
    }
    else {
      this.nameCusDirIsNULL = false;
    }

    for (var i = 0; i < nameArrNew.length; i++) {
      if (nameArrNew[i] != " ") {
        nameArr.push(nameArrNew[i]);
      }
    }
    for (var i = 0; i < nameArr.length; i++) {
      if (isNaN(Number(nameArr[i].toString())) == false) {
        isNum += 1;
      }
      if (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(nameArr[i])) {
        isSymbol += 1;
      }
    }

    if (isNum > 0 || isSymbol > 0) {
      this.nameCusDirErr = true;
    }
    else {
      this.nameCusDirErr = false;
      isSymbol = 0;
      isNum = 0;
    }

  }

  checkPhone() {
    var phoneArr = this.phone.split('');

    if (phoneArr.length == 0) {
      this.phoneIsNULL = true;
    }
    else {
      this.phoneIsNULL = false;
    }

    if (phoneArr.length != 10 && phoneArr.length != 0) {
      this.phoneErr = true;
    }
    else {
      this.phoneErr = false;
    }

  }

  checkPhoneCusDir() {
    var phoneArr = this.phoneCusDir.split('');

    if (phoneArr.length == 0) {
      this.phoneCusDirIsNULL = true;
    }
    else {
      this.phoneCusDirIsNULL = false;
    }

    if (phoneArr.length != 10 && phoneArr.length != 0) {
      this.phoneCusDirErr = true;
    }
    else {
      this.phoneCusDirErr = false;
    }

  }

  checkAddress() {
    var addressArr = this.address.split('');

    if (addressArr.length == 0) {
      this.addressIsNULL = true;
    }
    else {
      this.addressIsNULL = false;
    }
  }

  checkAddressCusDir() {
    var addressArr = this.addressCusDir.split('');

    if (addressArr.length == 0) {
      this.addressCusDirIsNULL = true;
    }
    else {
      this.addressCusDirIsNULL = false;
    }
  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }

  readVideoFileasGeneral(data_uri) {
    if (!data_uri.includes('file://')) {
      data_uri = 'file://' + data_uri;
    }
    return this.file.resolveLocalFilesystemUrl(data_uri)
      .then((entry: any) => {
        //***it does not get in here***
        // this.presentQuickToastMessage(data_uri); 
        return new Promise((resolve) => {//, reject) => { 
          entry.file((file) => {
            let fileReader = new FileReader();
            fileReader.onloadend = () => {
              let blob = new Blob([fileReader.result], { type: file.type });
              resolve({ blob: blob, file: file });
            };
            fileReader.readAsArrayBuffer(file);
          });
        })
      })
      .catch((error) => {
        // this.presentQuickToastMessage(error); 
        //***it presents "plugin_not_installed" here***
      });
  }

  changePayment() {
    console.log(this.payments);
    console.log(this.selectedRadio);
    for (var i = 0; i < this.payments.length; i++) {
      if (this.selectedRadio == this.payments[i].paymentType) {
        this.payment = this.payments[i];
        break;
      }
    }
  }

  async saveBook() {

    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    let currTime = moment().format("YYYYMMDDHHmmssms").toString();
    // this.id = Math.floor(Date.now() / 1000) + String(this.phone);


    console.log(this.id)

    let payment: any;
    if (this.payment.paymentStatusCode == '0') {
      payment = {
        status: '0',
        paymentMethod: '0',
        paymentDate: "",
        invoice: {}
      }
    }
    else {
      payment = {
        status: '0',
        paymentMethod: '1',
        paymentDate: "",
        invoice: {}
      }
    }

    // let BookData=[];
    this.localStorage.get('BookData').then((res) => {
      // this.deviceID=res;
      console.log(JSON.parse(res));
      let BookData = JSON.parse(res) || [];
      let data = {
        createdByDevice: "crm_app",
        bucket: "crm_app_order",
        creatingDate: moment().format("DD/MM/YYYY HH:mm:ss"),
        description: "",
        content: {
          id: this.id,
          customerInfo: {
            id: '123456',
            name: this.name,
            phone: this.phone,
            address: this.address + ", " + this.wardName + ", " + this.districtName + ", " + this.provinceName
          },
          customerDirectContact: {
            name: this.nameCusDir,
            phone: this.phoneCusDir,
            address: this.addressCusDir + ", " + this.wardNameCusDir + ", " + this.districtNameCusDir + ", " + this.provinceNameCusDir
          },
          chosenServices: this.selectedService,
          expectedTime: moment(this.datetime).format('DD/MM/YYYY HH:mm:ss'),
          expectedAddress: this.addressCusDir + ", " + this.wardNameCusDir + ", " + this.districtNameCusDir + ", " + this.provinceNameCusDir,
          hasContract: "0",
          quote_time: "",
          note: this.note,
          status: '0',
          mobileChannel: this.mobileChannel,
          deviceid: this.deviceID,
          paymentInfo: payment,
          orderValue: {
            sum: '',
            details: this.serviceProvider.selectedServiceList
          },
          imageList: [],
          videoList: '',
          assignInfo: {
            assignDate: '',
            assigner: {
              id: '',
              name: '',
              phone: ''
            },
            assignee: {
              id: '',
              name: '',
              phone: '',
              avatar: '',
              workDate: ''
            }
          }
        }
      }

      this.serviceProvider.createOrder(this.id, data).subscribe((res) => {
        console.log(res);
        // alert(this.deviceID);
        let toast = this.toastController.create({
          message: 'Đặt lịch thành công',
          duration: 1000,
          position: 'top'
        });

        toast.present();

        BookData.push(this.id);
        this.localStorage.set('BookData', JSON.stringify(BookData));

        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "0",
            orderID: this.id,
            msg: "Có đơn hàng mới"
          },
          isNew: "1"
        };

        let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

        this.serviceProvider.publishMessage(newMessage, id);
        // alert(this.deviceID);
        // alert(this.serviceProvider.idOfDevice);


        // this.sdk.notification.Publish("co lịch mới");

        if (this.imageResponse.length > 0)
          this.newImgsSender()
        if (this.video != '')
          this.videosSender();
        loader.dismiss();
        this.navCtrl.pop();
      }, err => {
        alert("Đặt lịch không thành công! Đặt lại");
        loader.dismiss();
      });
    })
  }

  //Các hàm xử lý ảnh, video
  takePicture() {
    let options: CameraOptions = {
      quality: 0,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    if (Array.isArray(this.imageResponse)) {
      if (this.imageResponse.length == 10) {
        alert(
          "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
        );
      } else {
        this.camera.getPicture(options).then(
          imageData => {
            console.log('file uri: ' + imageData)
            this.imageResponse.push(imageData)
          },
          err => {
            console.log(err);
          }
        );
      }
    }
    else {
      this.camera.getPicture(options).then(
        imageData => {
          console.log('file uri: ' + imageData)
        },
        err => {
          console.log(err);
        }
      );
    }

  }

  recordVideo() {
    if (this.video != "")
      alert(
        "Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options).then(
        (data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            this.video = data[i].fullPath;
            console.log(this.video)
          }
        },
        (err: CaptureError) => {
          console.error(err);
        }
      );
    }
  }

  getImages() {
    if (this.imageResponse.length == 10) {
      alert(
        "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let count = 10 - this.imageResponse.length;
      let options: ImagePickerOptions = {
        quality: 0,
        outputType: 0,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          for (let pic of results) {
            this.imageResponse.push(pic);
          }
          console.log(this.imageResponse)
        },
        err => {
          console.log(err)
        }
      );
    }
  }

  delPic(pic, index) {
    console.log('ảnh cần xoá: ' + pic)
    this.imageResponse.splice(index, 1);
  }

  getVideo() {
    console.log("video");
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.VIDEO
    };

    this.camera.getPicture(options).then((data) => {
      console.log(data)
      this.video = data
    }, (err) => {
      console.log(err)
    });
  }

  delVideo() {
    this.video = ''
  }

  fixDisplayURLFromFileURI(url: string) {
    let win: any = window;
    return win.Ionic.WebView.convertFileSrc(url)
  }

  async presentActionPhoto() {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [{
        text: 'Chụp ảnh mới',
        icon: 'camera',
        handler: () => {
          console.log('Delete clicked');
          this.takePicture();
        }
      },
      {
        text: 'Lấy từ thư viện',
        icon: 'images',
        handler: () => {
          console.log('Share clicked');
          this.getImages();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  async presentActionVideo() {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [{
        text: 'Quay mới',
        icon: 'videocam',
        handler: () => {
          console.log('Delete clicked');
          this.recordVideo();
        }
      },
      {
        text: 'Lấy từ thư viện',
        icon: 'film',
        handler: () => {
          console.log('Share clicked');
          this.getVideo();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  newImgsSender() {
    for (let i = 0; i < this.imageResponse.length; ++i) {
      let pic = this.imageResponse[i]
      console.log('link ảnh:' + pic);
      if (pic.startsWith("file")) {
        this.file
          .resolveLocalFilesystemUrl(pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      } else {
        this.file
          .resolveLocalFilesystemUrl("file://" + pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      }

    }
  }

  private readnewImgFile(file: any, index) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      console.log('append to form')
      this.masterFormdataToSend.append("data", imgBlob, Math.abs(new Date().getTime()).toString())
      if (index == this.imageResponse.length - 1) {
        let self = this;
        setTimeout(function () {
          console.log("Timeout");
          self.postNewImgData(self.masterFormdataToSend, 'abc')
        }, 3000);

      }
    };
    reader.readAsArrayBuffer(file);
  }

  private postNewImgData(formData: FormData, filename) {
    console.log('tên file: ' + filename);
    console.log("Here and ready to be uploaded - new img upload");
    console.log(formData.getAll('data'))

    let url = this.serviceProvider.apiURL.uploadFile
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA",
      // userid: "user1",
      // deviceid: "crm_app",
      Authorization: `Bearer ${this.serviceProvider.token}`
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .subscribe(
        data => {
          console.log(data)
          console.log('gửi ảnh thành công');
          let a = <any>data
          console.log(JSON.parse(a._body))
          let arrayTosend = []
          for (let img of JSON.parse(a._body)) {
            console.log(img.path);
            arrayTosend.push(img.path)
          }
          if (arrayTosend.length > 0)
            this.serviceProvider.saveImgVideotoOrder(this.id, arrayTosend, null)
        },
        err => {
          console.log('gửi ảnh fail');
          console.log(err)
        }
      );
  }

  videosSender() {

    if (this.video.startsWith("file")) {
      this.file
        .resolveLocalFilesystemUrl(this.video)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            console.log("video size : " + metadata.size);
            (entry as FileEntry).file(file => this.readFile(file));
          });
        })
        .catch(err => console.log(err));
    } else {
      this.file
        .resolveLocalFilesystemUrl("file://" + this.video)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            (entry as FileEntry).file(file => this.readFile(file));
          });
        })
        .catch(err => console.log(err));
    }
  }

  private readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      formData.append("data", imgBlob, Math.abs(new Date().getTime()).toString());
      this.postData(formData, file.name);
    };
    reader.readAsArrayBuffer(file);
  }

  private postData(formData: FormData, filename) {
    console.log("Here and ready to be uploaded");
    let url = this.serviceProvider.apiURL.uploadFile
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA",
      // userid: "user1",
      // deviceid: "crm_app",
      Authorization: `Bearer ${this.serviceProvider.token}`
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .subscribe(
        data => {
          console.log(data)
          console.log('gửi video thành công');
          let a = <any>data
          console.log(JSON.parse(a._body))
          let arrayTosend = []
          for (let img of JSON.parse(a._body)) {
            console.log(img.path);
            arrayTosend.push(img.path)
          }
          if (arrayTosend.length > 0)
            this.serviceProvider.saveImgVideotoOrder(this.id, null, arrayTosend[0])
        },
        err => {
          console.log('gửi video fail')
          console.log(err)
        }
      );
  }

  changeProvince(e) {
    this.districtName = '';
    this.wardName = '';
    for (var i = 0; i < this.provinces.length; i++) {
      if (this.provinces[i].province_code == e) {
        this.provinceName = this.provinces[i].province_name;
      }
    }
    this.serviceProvider.getDistrictsInProvince(e)
      .subscribe(data => {
        let districtNumberArr = [];
        let districtNameArr = [];
        let townArr = [];
        let result = JSON.parse(JSON.stringify(data)).data_item;

        for (var j = 0; j < result.length; j++) {
          let str = result[j].district_name.toLowerCase().split(' ');

          if (str[0] == 'quận') {
            if (!isNaN(Number(str[str.length - 1]))) {
              districtNumberArr.push(result[j]);
            }
            if (isNaN(Number(str[str.length - 1]))) {
              districtNameArr.push(result[j]);
            }
          }
          else {
            townArr.push(result[j]);
          }
        }

        districtNumberArr = this.sortByDistrictNumber(districtNumberArr);
        districtNameArr = this.sortByDistrictName(districtNameArr);
        townArr = this.sortByDistrictName(townArr);

        this.districts = districtNumberArr.concat(districtNameArr, townArr);
      });
  }

  changeDistrict(e) {
    this.districtIsNULL = false;
    this.wardName = '';
    let neighborhoodNameArr = [];
    let neighborhoodNumberArr = [];
    let villageArr = [];

    for (var i = 0; i < this.districts.length; i++) {
      if (this.districts[i].district_code == e) {
        this.districtName = this.districts[i].district_name;
      }
    }


    this.serviceProvider.getWardsInDistrict(this.selectedProvince, e).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data)).data_item;
      console.log(result);

      for (let res of result) {
        let arr = res.ward_name.split(' ');
        if (arr[0] == 'phường') {
          if (!isNaN(Number(arr[arr.length - 1]))) {
            neighborhoodNumberArr.push(res);
          }
          else {
            neighborhoodNameArr.push(res);
          }
        }
        else {
          villageArr.push(res);
        }
      }

      neighborhoodNameArr = this.sortByVillageName(neighborhoodNameArr);
      neighborhoodNumberArr = this.sortByVillageNumber(neighborhoodNumberArr);
      villageArr = this.sortByVillageName(villageArr);
      console.log(neighborhoodNameArr);
      console.log(neighborhoodNumberArr);
      console.log(villageArr);

      this.wards = neighborhoodNumberArr.concat(neighborhoodNameArr, villageArr);
    })
  }

  changeWard(e) {
    this.wardIsNULL = false;
    for (var i = 0; i < this.wards.length; i++) {
      if (this.wards[i].ward_code == e) {
        this.wardName = this.wards[i].ward_name;
      }
    }
  }

  sortByDistrictName(arr) {
    let arrName = arr.sort((a, b) => {
      let t1 = a.district_name.toLowerCase();
      let t2 = b.district_name.toLowerCase();

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    })

    return arrName;
  }

  sortByDistrictNumber(arr) {
    let arrNumber = arr.sort((a, b) => {
      let t1 = Number(a.district_name.match(/\d+/)[0]);
      let t2 = Number(b.district_name.match(/\d+/)[0]);

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    });
    return arrNumber;
  }

  sortByVillageName(arr) {
    let arrName = arr.sort((a, b) => {
      let t1 = a.ward_name.toLowerCase();
      let t2 = b.ward_name.toLowerCase();

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    })

    return arrName;
  }

  sortByVillageNumber(arr) {
    let arrNumber = arr.sort((a, b) => {
      let t1 = Number(a.ward_name.match(/\d+/)[0]);
      let t2 = Number(b.ward_name.match(/\d+/)[0]);

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    });
    return arrNumber;
  }

  changeProvinceCusDir(e) {
    this.districtNameCusDir = '';
    this.wardNameCusDir = '';
    for (var i = 0; i < this.provincesCusDir.length; i++) {
      if (this.provincesCusDir[i].province_code == e) {
        this.provinceNameCusDir = this.provincesCusDir[i].province_name;
      }
    }
    this.serviceProvider.getDistrictsInProvince(e).subscribe(data => {
      this.districtsCusDir = JSON.parse(JSON.stringify(data)).data_item;
    });
  }

  changeDistrictCusDir(e) {
    this.districtIsNULLCusDir = false;
    this.wardNameCusDir = '';

    for (var i = 0; i < this.districtsCusDir.length; i++) {
      if (this.districtsCusDir[i].district_code == e) {
        this.districtNameCusDir = this.districtsCusDir[i].district_name;
      }
    }
    this.serviceProvider.getWardsInDistrict(this.selectedProvinceCusDir, e).subscribe(data => {
      this.wardsCusDir = JSON.parse(JSON.stringify(data)).data_item;
    })
  }

  changeWardCusDir(e) {
    this.wardIsNULLCusDir = false;
    for (var i = 0; i < this.wardsCusDir.length; i++) {
      if (this.wardsCusDir[i].ward_code == e) {
        this.wardNameCusDir = this.wardsCusDir[i].ward_name;
      }
    }
  }
}
