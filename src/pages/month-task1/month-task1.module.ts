import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MonthTask1Page } from './month-task1';
// import { NgCalendarModule  } from 'ionic2-calendar';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    MonthTask1Page,
  ],
  imports: [
    // NgCalendarModule,
    CalendarModule,
    IonicPageModule.forChild(MonthTask1Page),
  ],
})
export class MonthTask1PageModule {}
