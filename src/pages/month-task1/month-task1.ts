import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import moment from 'moment';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';


@Component({
  selector: 'page-month-task1',
  templateUrl: 'month-task1.html',
})
export class MonthTask1Page {

  date: string = new Date().toString();
  viewTitle = moment().format("DD/MM/YYYY")
  type: 'string';// 'string' | 'js-date' | 'moment' | 'time' | 'object'
  task = [];
  mon
  daysConfig: any = [];
  dataOfCalendar = [];
  optionsMulti: CalendarModalOptions = {
    canBackwardsSelected: true,
    daysConfig: this.daysConfig
    // color:'primary',
  };
  monthTask: any = [];
  nextMonthTask: any = [];
  pastDay = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController, public serviceProvider: ServiceProvider) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.date = new Date().toString();
    console.log("curUser", this.serviceProvider.currentUser)
    if (this.serviceProvider.currentUser != '') {
      this.getThisMonth();
    }
    else {
      console.log("đã đăng xuất");
      this.daysConfig = [];
      this.task = [];
      this.monthTask = [];
      this.dataOfCalendar = [];
      this.optionsMulti = {};
    }

  }


  getThisMonth() {
    this.task = [];
    this.monthTask = [];
    this.dataOfCalendar = [];
    this.daysConfig = [];

    this.serviceProvider.getMonthTask().subscribe((data) => {
      let res = <any>data;
      this.monthTask = res;
      console.log(this.monthTask);

      for (let order of res) {
        let checkExistence = false
        for (let dateData of this.dataOfCalendar) {
          if (moment(order.content.assignInfo.assignee.workDate).format("DD/MM/YYYY") == moment(dateData.date).format("DD/MM/YYYY")) {
            dateData.data.push(order);
            checkExistence = true;
            break;
          }
        }
        if (checkExistence == false) {
          this.dataOfCalendar.push({
            date: moment(order.content.assignInfo.assignee.workDate).format("DD/MM/YYYY"),
            data: [order]
          })
        }
      }
      console.log('Du lieu cho lich thang')
      console.log(this.dataOfCalendar);
      for (let dt of this.dataOfCalendar) {
        this.daysConfig.push({
          cssClass: 'marked',
          date: new Date(moment(dt.date, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY")),
          subTitle: dt.data.length,
          marked: true
        })
      }
      this.getNextMonth()
    });
  }
  getNextMonth() {
    this.serviceProvider.getNextMonthTask().subscribe((data) => {
      let res = <any>data;
      this.nextMonthTask = res;
      console.log(this.nextMonthTask);

      for (let order of res) {
        let checkExistence = false
        for (let dateData of this.dataOfCalendar) {
          if (moment(order.content.assignInfo.assignee.workDate).format("DD/MM/YYYY") == moment(dateData.date).format("DD/MM/YYYY")) {
            dateData.data.push(order);
            checkExistence = true;
            break;
          }
        }
        if (checkExistence == false) {
          this.dataOfCalendar.push({
            date: moment(order.content.assignInfo.assignee.workDate).format("DD/MM/YYYY"),
            data: [order]
          })
        }
      }

      for (let dt of this.dataOfCalendar) {
        this.daysConfig.push({
          cssClass: 'marked',
          date: new Date(moment(dt.date, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY")),
          subTitle: dt.data.length,
          marked: true
        })
      }
      this.optionsMulti = {
        daysConfig: this.daysConfig,
        canBackwardsSelected: true
        // color:'primary',
      };

      this.onChange(event);
    })
  }

  today() {
    this.task = [];
    // this.monthTask=[];
    // this.dataOfCalendar=[];
    this.date = new Date().toString();
    this.viewTitle = moment().format("DD/MM/YYYY");

    for (let dt of this.dataOfCalendar) {
      if (moment().format("DD/MM/YYYY") == moment(dt.date).format("DD/MM/YYYY")) {
        for (let task of dt.data) {
          this.task.push(task);
        }
      }
    }
  }

  onChange($event) {
    this.task = [];
    this.viewTitle = moment(this.date.toString()).format("DD/MM/YYYY");

    if (moment(this.date).format("DD/MM/YYYY") >= moment().format("DD/MM/YYYY")) {
      this.pastDay = false;

      for (let dt of this.dataOfCalendar) {
        if (moment(this.date).format("DD/MM/YYYY") == moment(dt.date).format("DD/MM/YYYY")) {
          for (let task of dt.data) {
            this.task.push(task);
          }
        }
      }
    }
    else {
      this.pastDay = true;

      for (let dt of this.dataOfCalendar) {
        if (moment(this.date).format("DD/MM/YYYY") == moment(dt.date).format("DD/MM/YYYY")) {
          for (let task of dt.data) {
            if (task.content.status != '5')
              this.task.push(task);
          }

          for (let task of dt.data) {
            if (task.content.status == '5')
              this.task.push(task);
          }
        }
      }

    }

  }

  toDetail(i) {
    var data = this.task[i];
    this.navCtrl.push(DetailTaskPage, { dt: data });
    console.log("dfgd", data)
  }

}
