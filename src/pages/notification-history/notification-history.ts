import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../services/GlobalVar';
import { ServicesPricePage } from '../services-price/services-price';

@Component({
  selector: 'page-notification-history',
  templateUrl: 'notification-history.html',
})
export class NotificationHistoryPage {

  data;
  isNull = false;

  constructor(public localStorage: Storage, public navCtrl: NavController, public serviceProvider: ServiceProvider, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter NotificationHistoryPage');
    this.data = JSON.parse(localStorage.getItem("Notification_History")) || [];
    // console.log(JSON.parse(localStorage.getItem("Notification_History")));
    console.log(this.data);

    if (this.data.length == 0) {
      this.isNull = true;
      console.log(this.data);
    }
    else {
      this.isNull = false;
    }
  }

  goDetail(id) {
    this.serviceProvider.getOrder(id).subscribe(res => {
      console.log(res);
      let data = <any>res;
      // let data = JSON.parse(dt);
      this.navCtrl.push(ServicesPricePage, { dt: data.data_item, id: 1 });
    })
  }
}
