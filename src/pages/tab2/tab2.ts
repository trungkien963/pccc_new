import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TechniciansPage } from '../technicians/technicians';
import { MonthTask1Page } from '../month-task1/month-task1';
import { HistoryTaskPage } from '../history-task/history-task';
import { AccountPage } from '../account/account'


@Component({
  selector: 'page-tab2',
  templateUrl: 'tab2.html',
})
export class Tab2Page {
  tabHomeRoot: any = TechniciansPage;
  tabMonthTaskRoot: any = MonthTask1Page;
  tabHistoryTaskRoot: any = HistoryTaskPage;
  tabAccountTaskRoot: any = AccountPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tab2Page');
  }

  ionViewDidLeave() {
    this.navCtrl.popToRoot();
  }

}
