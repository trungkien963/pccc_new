import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})
export class PayPage {
  data: any;

  constructor(public toastController: ToastController, public navCtrl: NavController, public navParams: NavParams, public serviceProvider: ServiceProvider) {
    this.data = this.navParams.get('dt');
  }

}
