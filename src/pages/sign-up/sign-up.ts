import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../services/GlobalVar';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  email: string = "";
  password: string = "";
  name: string = "";
  isLogin = false;
  loginErr = false;
  type = 'password';
  nameErr = false;
  emailErr = false;
  passwordErr = false;
  displayErrName = false;
  displayErrEmail = false;
  img = '../../assets/imgs/LOGO SAFE HOUSE.png';
  phone: string = "";
  displayErrPhone = false;
  phoneErr = false;
  selectedProvince = "79";
  provinceName = "Thành phố Hồ Chí Minh";
  provinces = [];
  selectedDistrict;
  districts = [];
  districtIsNULL = false;
  selectedWard;
  wards = [];
  wardIsNULL = false;
  address: string;
  addressIsNULL = false;
  districtName: string;
  wardName: string;

  constructor(public localStorage: Storage, public navCtrl: NavController, public serviceProvider: ServiceProvider, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
    this.serviceProvider.getProvinces().subscribe(data => {
      console.log("Tỉnh thành", data.json());
      this.provinces = data.json().data_item;
      for (var i = 0; i < this.provinces.length; i++) {
        if (this.provinces[i].province_code == "79") {
          let temp = this.provinces[0];
          this.provinces[0] = this.provinces[i];
          this.provinces[i] = temp;
        }
        if (this.provinces[i].province_code == "01") {
          let temp = this.provinces[1];
          this.provinces[1] = this.provinces[i];
          this.provinces[i] = temp;
        }
      }
    });

    this.changeProvince(this.selectedProvince);
  }

  changeType() {
    if (this.type == 'password') {
      this.type = 'text';
    }
    else {
      this.type = 'password';
    }
  }

  changeName() {
    this.displayErrName = true;
    var nameArr = [];
    var nameArrNew = this.name.split('');
    let isNum = 0;
    let isSymbol = 0;

    if (nameArrNew.length == 0) {
      this.nameErr = true;
    }
    else {
      for (var i = 0; i < nameArrNew.length; i++) {
        if (nameArrNew[i] != " ") {
          nameArr.push(nameArrNew[i]);
        }
      }
      for (var i = 0; i < nameArr.length; i++) {
        if (isNaN(Number(nameArr[i].toString())) == false) {
          isNum += 1;
        }
        if (/[!@#$%^&*()_+\-~`=\[\]{};':"\\|,.<>\/?]/.test(nameArr[i])) {
          isSymbol += 1;
        }
      }

      if (isNum > 0 || isSymbol > 0) {
        this.nameErr = true;
      }
      else {
        this.nameErr = false;
        isSymbol = 0;
        isNum = 0;
      }
    }
  }

  changeEmail() {
    this.displayErrEmail = true;
    if (this.validateEmail(this.email)) {
      this.emailErr = false;
    }
    else {
      this.emailErr = true;
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onSignup() {
    if (this.address == null) {
      this.addressIsNULL = true;
    }

    if (this.districtName == '') {
      this.districtIsNULL = true;
    }

    if (this.wardName == '') {
      this.wardIsNULL = true;
    }

    this.changeEmail();
    this.changeName();
    this.changePhone();
    this.checkPassword();

    if (this.emailErr == false && this.nameErr == false && this.phoneErr == false && this.passwordErr == false && this.addressIsNULL == false && this.districtIsNULL == false && this.wardIsNULL == false) {
      let address = this.address + ", " + this.wardName + ", " + this.districtName + ", " + this.provinceName;

      let name = this.name.split(" ");
      let first_name = name[name.length - 1];
      let lastName = name.slice(0, -1);
      let last_name = "";

      for (let a of lastName) {
        last_name = last_name + a + " ";
      }

      let data = {
        username: this.phone,
        password: this.password,
        first_name: first_name,
        last_name: last_name,
        phone_number: this.phone,
        address: address,
        email: this.email,
        type: ""
      }

      this.serviceProvider.register(data).subscribe(res => {
        let result = <any>res;
        if (result.code == "0") {
          alert("Đăng ký tài khoản thành công");

          let account = {
            username: this.phone,
            name: this.name,
            status: 1,
            roles: "end_user",
            address: address,
          }
          this.localStorage.set("Account", account);
          console.log('Access!');
          this.serviceProvider.isLogin = true;
          this.serviceProvider.currentUser = this.phone;
          this.serviceProvider.isCustomer = true;
          this.navCtrl.setRoot(TabsPage);
          this.navCtrl.popToRoot();
        }
      });
    }
  }

  back() {
    this.navCtrl.pop();
  }

  checkPassword() {
    if (this.password == "") {
      this.passwordErr = true;
    }
    else {
      this.passwordErr = false;
    }
  }

  changePhone() {
    this.displayErrPhone = true;
    let phoneArr = this.phone.split("");

    if (phoneArr.length == 0) {
      this.phoneErr = true;
    }
    else {
      if (phoneArr.length != 10 && phoneArr.length != 0) {
        this.phoneErr = true;
      }
      else {
        this.phoneErr = false;
      }
    }
  }

  changeProvince(e) {
    this.districtName = '';
    this.wardName = '';
    for (var i = 0; i < this.provinces.length; i++) {
      if (this.provinces[i].province_code == e) {
        this.provinceName = this.provinces[i].province_name;
      }
    }
    this.serviceProvider.getDistrictsInProvince(e)
      .subscribe(data => {
        let districtNumberArr = [];
        let districtNameArr = [];
        let townArr = [];
        let result = JSON.parse(JSON.stringify(data)).data_item;

        for (var j = 0; j < result.length; j++) {
          let str = result[j].district_name.toLowerCase().split(' ');

          if (str[0] == 'quận') {
            if (!isNaN(Number(str[str.length - 1]))) {
              districtNumberArr.push(result[j]);
            }
            if (isNaN(Number(str[str.length - 1]))) {
              districtNameArr.push(result[j]);
            }
          }
          else {
            townArr.push(result[j]);
          }
        }

        districtNumberArr = this.sortByDistrictNumber(districtNumberArr);
        districtNameArr = this.sortByDistrictName(districtNameArr);
        townArr = this.sortByDistrictName(townArr);

        this.districts = districtNumberArr.concat(districtNameArr, townArr);
      });
  }

  changeDistrict(e) {
    this.districtIsNULL = false;
    this.wardName = '';
    let neighborhoodNameArr = [];
    let neighborhoodNumberArr = [];
    let villageArr = [];

    for (var i = 0; i < this.districts.length; i++) {
      if (this.districts[i].district_code == e) {
        this.districtName = this.districts[i].district_name;
      }
    }


    this.serviceProvider.getWardsInDistrict(this.selectedProvince, e).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data)).data_item;
      console.log(result);

      for (let res of result) {
        let arr = res.ward_name.split(' ');
        if (arr[0] == 'phường') {
          if (!isNaN(Number(arr[arr.length - 1]))) {
            neighborhoodNumberArr.push(res);
          }
          else {
            neighborhoodNameArr.push(res);
          }
        }
        else {
          villageArr.push(res);
        }
      }

      neighborhoodNameArr = this.sortByVillageName(neighborhoodNameArr);
      neighborhoodNumberArr = this.sortByVillageNumber(neighborhoodNumberArr);
      villageArr = this.sortByVillageName(villageArr);
      console.log(neighborhoodNameArr);
      console.log(neighborhoodNumberArr);
      console.log(villageArr);

      this.wards = neighborhoodNumberArr.concat(neighborhoodNameArr, villageArr);
    })
  }

  changeWard(e) {
    this.wardIsNULL = false;
    for (var i = 0; i < this.wards.length; i++) {
      if (this.wards[i].ward_code == e) {
        this.wardName = this.wards[i].ward_name;
      }
    }
  }

  sortByDistrictName(arr) {
    let arrName = arr.sort((a, b) => {
      let t1 = a.district_name.toLowerCase();
      let t2 = b.district_name.toLowerCase();

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    })

    return arrName;
  }

  sortByDistrictNumber(arr) {
    let arrNumber = arr.sort((a, b) => {
      let t1 = Number(a.district_name.match(/\d+/)[0]);
      let t2 = Number(b.district_name.match(/\d+/)[0]);

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    });
    return arrNumber;
  }

  sortByVillageName(arr) {
    let arrName = arr.sort((a, b) => {
      let t1 = a.ward_name.toLowerCase();
      let t2 = b.ward_name.toLowerCase();

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    })

    return arrName;
  }

  sortByVillageNumber(arr) {
    let arrNumber = arr.sort((a, b) => {
      let t1 = Number(a.ward_name.match(/\d+/)[0]);
      let t2 = Number(b.ward_name.match(/\d+/)[0]);

      if (t1 > t2) {
        return 1;
      }
      if (t1 < t2) {
        return -1;
      }

      return 0;
    });
    return arrNumber;
  }

  checkAddress() {
    var addressArr = this.address.split('');

    if (addressArr.length == 0) {
      this.addressIsNULL = true;
    }
    else {
      this.addressIsNULL = false;
    }
  }

}
