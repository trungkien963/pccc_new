import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HistoryBookPage } from '../history-book/history-book'; 
import { NotificationHistoryPage } from '../notification-history/notification-history';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tabHomeRoot: any = HomePage;
  tabHistoryRoot: any = HistoryBookPage;
  tabNotificationRoot:any = NotificationHistoryPage;
  tabContactRoot:any = ContactPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
