import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
// import { company } from '../../services/company';
import { ServiceProvider } from '../../services/GlobalVar';
import { CallNumber } from '@ionic-native/call-number';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  // company = company;
  company=[];
  phone:number;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertController: AlertController, 
    public serviceProvider:ServiceProvider, private callNumber: CallNumber) {
    this.serviceProvider.getCompanyInfo().subscribe(data=>{
      console.log(data);
      let res = <any>data;
      this.company=res.data_item.content;
      console.log(this.company);
      this.phone=res.data_item.content.hotline;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  async precall() {
    const alert = await this.alertController.create({
      title: 'Gọi',
      message: `${this.phone}`,
      buttons: [
        {
          text: 'Hủy',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            this.callNumber.callNumber(this.phone.toString(), true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
          }
        }
      ]
    });

    await alert.present();
  }

}
