import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { ServicesPricePage } from '../services-price/services-price';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  orders = [];
  isNull = false;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams,
    public modal: ModalController, public localStorage: Storage) {
  }

  async ionViewWillEnter() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.orders = [];
    await this.localStorage.get("notificationHistory").then(res => {
      loader.dismiss();
      if (res.length != 0) {
        console.log(JSON.parse(res))
        console.log(res.length)
        if (JSON.parse(res).length == 0)
          this.isNull = true;
        else {
          for (let i = JSON.parse(res).length - 1; i >= 0; i--) {
            this.orders.push(JSON.parse(res)[i])
          }
        }
      }
      else {
        this.isNull = true;
      }

    });
  }

  async doRefresh(refresher) {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.orders = [];
    await this.localStorage.get("notificationHistory").then(res => {
      refresher.complete();
      loader.dismiss();
      console.log(JSON.parse(res))
      console.log(res.length)
      if (JSON.parse(res).length == 0)
        this.isNull = true;
      else {
        for (let i = JSON.parse(res).length - 1; i >= 0; i--) {
          this.orders.push(JSON.parse(res)[i])
        }
      }
    });
  }

  toServicesPrice(v: number) {
    var data = this.orders[v].order;
    // let myModal = this.navCtrl.push(ServicesPricePage, { dt: data, id: v });
    this.navCtrl.push(ServicesPricePage, { dt: data, id: v });
    // myModal.present();
  }

}
